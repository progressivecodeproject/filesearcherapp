﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using FileSearcherApp.Properties;

namespace FileSearcherApp
{
    /// <summary>
    /// Class data for send event.
    /// </summary>
    class FSEventData
    {
        /// <summary>
        /// Path to file.
        /// </summary>
        public string Path { get; set; } = string.Empty;

        /// <summary>
        /// Number of all files.
        /// </summary>
        public int FilesAllCount { get; set; } = 0;

        /// <summary>
        /// Number of checked files.
        /// </summary>
        public int CheckedFiles { get; set; } = 0;

        /// <summary>
        /// Number of found files.
        /// </summary>
        public int FoundFiles { get; set; } = 0;

        /// <summary>
        /// Elapsed time since start search.
        /// </summary>
        public Stopwatch Time { get; set; } = new Stopwatch();
    }

    /// <summary>
    /// Represents the method that handles the event.
    /// </summary>
    /// <param name="data">An object containing event data.</param>
    delegate void FSEvent(FSEventData data);

    /// <summary>
    /// File search class
    /// </summary>
    class FileSearcher : IDisposable
    {
        /// <summary>
        /// Path to the directory where the search begins.
        /// </summary>
        public string StartDirectory { get; set; } = _StartDirectory;

        /// <summary>
        /// Path to the directory where the search begins. Changing this parameter is immediately saved.
        /// </summary>
        static public string _StartDirectory
        {
            get => Settings.Default.StartDirectory;
            set
            {
                Settings.Default.StartDirectory = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Pattern to search by name.
        /// </summary>
        public string SearchPattern { get; set; } = _SearchPattern;

        /// <summary>
        /// Pattern to search by name. Changing this parameter is immediately saved.
        /// </summary>
        static public string _SearchPattern
        {
            get => Settings.Default.SearchPattern;
            set
            {
                Settings.Default.SearchPattern = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Text to search by file content.
        /// </summary>
        public string SearchContent { get; set; } = _SearchContent;

        /// <summary>
        /// Text to search by file content. Changing this parameter is immediately saved.
        /// </summary>
        static public string _SearchContent
        {
            get => Settings.Default.SearchContent;
            set
            {
                Settings.Default.SearchContent = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Search file by name? Default: true.
        /// </summary>
        public bool IsFindByName { get; set; } = _IsFindByName;

        /// <summary>
        /// Search file by name? Default: true. Changing this parameter is immediately saved.
        /// </summary>
        static public bool _IsFindByName
        {
            get => Settings.Default.IsFindByName;
            set
            {
                Settings.Default.IsFindByName = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Search file by content? Default: false.
        /// </summary>
        public bool IsFindByContent { get; set; } = _IsFindByContent;

        /// <summary>
        /// Search file by content? Default: false. Changing this parameter is immediately saved.
        /// </summary>
        static public bool _IsFindByContent
        {
            get => Settings.Default.IsFindByContent;
            set
            {
                Settings.Default.IsFindByContent = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Event on begin of the search
        /// </summary>
        public FSEvent eventStart { get; set; } = null;

        /// <summary>
        /// Event on pause of the search.
        /// </summary>
        public FSEvent eventPause { get; set; } = null;

        /// <summary>
        /// Event on resume of the search.
        /// </summary>
        public FSEvent eventResume { get; set; } = null;

        /// <summary>
        /// Event on end of the search.
        /// </summary>
        public FSEvent eventEnd { get; set; } = null;

        /// <summary>
        /// File handling event.
        /// </summary>
        public FSEvent eventProcessing { get; set; } = null;

        /// <summary>
        /// Successful event in the search files.
        /// </summary>
        public FSEvent eventSuccess { get; set; } = null;

        /// <summary>
        /// Regular expression to find the file by name.
        /// </summary>
        private Regex regex { get; set; }

        /// <summary>
        /// Thread of worker.
        /// </summary>
        private Thread thread { get; set; } = null;

        /// <summary>
        /// Enums statuses worker.
        /// </summary>
        private enum WorkerState
        {
            /// <summary>
            /// Free worker.
            /// </summary>
            None,
            /// <summary>
            /// Worker is working.
            /// </summary>
            Run,
            /// <summary>
            /// Worker stopped.
            /// </summary>
            Pause,
            /// <summary>
            /// Worker finished work.
            /// </summary>
            End
        }

        /// <summary>
        /// Class containing information about the worker.
        /// </summary>
        private class WorkerData
        {
            /// <summary>
            /// State of the worker.
            /// </summary>
            public WorkerState State { get; set; } = WorkerState.None;

            /// <summary>
            /// Class data for send event.
            /// </summary>
            public FSEventData eventData { get; set; } = new FSEventData();

            /// <summary>
            /// Event on begin of the search
            /// </summary>
            public FSEvent eventStart { get; set; } = null;

            /// <summary>
            /// Event on end of the search.
            /// </summary>
            public FSEvent eventEnd { get; set; } = null;

            /// <summary>
            /// File handling event.
            /// </summary>
            public FSEvent eventProcessing { get; set; } = null;

            /// <summary>
            /// Successful event in the search files
            /// </summary>
            public FSEvent eventSuccess { get; set; } = null;

            /// <summary>
            /// Set all events to null.
            /// </summary>
            public void ClearEvents()
            {
                this.eventStart = null;
                this.eventProcessing = null;
                this.eventSuccess = null;
                this.eventEnd = null;
            }
        }

        /// <summary>
        /// Describing the state of the worker.
        /// </summary>
        private WorkerData workerData { get; set; } = new WorkerData();

        /// <summary>
        /// Class initialization.
        /// </summary>
        public FileSearcher()
        {
            
        }

        /// <summary>
        /// Start new search files.
        /// </summary>
        public void Start()
        {
            if (eventSuccess is null)
                throw new Exception("eventSuccess null.");
            if (!this.IsFindByName && !this.IsFindByContent)
                throw new Exception("Search filters are disabled.");
            if (!Directory.Exists(this.StartDirectory))
                throw new Exception("Directory not found.");
            string pattern = this.SearchPattern;
            if (pattern.StartsWith("*") || pattern.StartsWith("?")) pattern = "." + pattern;
            if (!VerifyRegex(pattern))
                throw new Exception("Regex pattern error.");
            try
            {
                this.regex = new Regex(pattern, RegexOptions.IgnoreCase);
                if (this.workerData.State == WorkerState.Run)
                {
                    this.workerData.State = WorkerState.End;
                    this.workerData.ClearEvents();
                }
                if (this.workerData.State == WorkerState.Pause)
                {
                    this.workerData.State = WorkerState.End;
                    this.workerData.ClearEvents();
                    this.thread.Resume();
                }
                thread = new Thread(Worker)
                {
                    IsBackground = true
                };
                thread.Start(this.workerData = new WorkerData()
                {
                    State = WorkerState.Run,
                    eventStart = this.eventStart,
                    eventProcessing = this.eventProcessing,
                    eventSuccess = this.eventSuccess,
                    eventEnd = this.eventEnd
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Auto pause or resume search files.
        /// </summary>
        public void AutoPauseOrResume()
        {
            try
            {
                if (this.workerData.State == WorkerState.Run)
                {
                    this.Pause();
                }
                else if (this.workerData.State == WorkerState.Pause)
                {
                    this.Resume();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Pause search files.
        /// </summary>
        public void Pause()
        {
            try
            {
                if (this.workerData.State != WorkerState.Run)
                    return;
                this.thread.Suspend();
                this.workerData.eventData.Time.Stop();
                this.workerData.State = WorkerState.Pause;
                this.eventPause?.Invoke(this.workerData.eventData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Resume seacrh files.
        /// </summary>
        public void Resume()
        {
            try
            {
                if (this.workerData.State != WorkerState.Pause)
                    return;
                this.workerData.State = WorkerState.Run;
                this.workerData.eventData.Time.Start();
                this.thread.Resume();
                this.eventResume?.Invoke(this.workerData.eventData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Save app config.
        /// </summary>
        public void Save()
        {
            try
            {
                _StartDirectory = this.StartDirectory;
                _SearchPattern = this.SearchPattern;
                _SearchContent = this.SearchContent;
                _IsFindByName = this.IsFindByName;
                _IsFindByContent = this.IsFindByName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Resource release and save app config.
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (this.workerData.State == WorkerState.Run)
                {
                    this.workerData.State = WorkerState.End;
                    this.workerData.ClearEvents();
                }
                if (this.workerData.State == WorkerState.Pause)
                {
                    this.workerData.State = WorkerState.End;
                    this.workerData.ClearEvents();
                    this.thread.Resume();
                }
                this.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Thread for search files.
        /// </summary>
        /// <param name="buffer">Describing the state of the worker.</param>
        private void Worker(object buffer)
        {
            try
            {
                WorkerData workerData = buffer as WorkerData;
                workerData.eventData.Time.Restart();
                string[] pathes = Directory.GetFiles(this.StartDirectory, "*", SearchOption.AllDirectories);
                workerData.eventData.FilesAllCount = pathes.Length;
                workerData.eventData.CheckedFiles = 0;
                workerData.eventData.FoundFiles = 0;
                workerData.eventStart?.Invoke(workerData.eventData);
                bool checkFile = false;
                foreach (string path in pathes)
                {
                    if (workerData.State == WorkerState.End)
                        break;
                    workerData.eventData.Path = path;
                    workerData.eventProcessing?.Invoke(workerData.eventData);
                    checkFile = this.CheckFile(path);
                    workerData.eventData.CheckedFiles++;
                    if (checkFile)
                    {
                        workerData.eventData.FoundFiles++;
                        workerData.eventSuccess?.Invoke(workerData.eventData);
                    }
                    Thread.Sleep(1);
                }
                workerData.eventData.Time.Stop();
                workerData.eventEnd?.Invoke(workerData.eventData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Check regular expression pattern is valid.
        /// </summary>
        /// <param name="pattern">Regular expression pattern.</param>
        private bool VerifyRegex(string pattern)
        {
            bool isValid = true;
            if ((pattern != null) && (pattern.Trim().Length > 0))
            {
                try
                {
                    Regex.Match("", pattern);
                }
                catch (ArgumentException)
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }
            return isValid;
        }

        /// <summary>
        /// Check if the file matches the search criteria.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        private bool CheckFile(string path)
        {
            try
            {
                bool isFindByName = false;
                bool isFindByContent = false;
                if (this.IsFindByName)
                {
                    isFindByName = this.regex.IsMatch(Path.GetFileName(path));
                }
                if (!isFindByName && this.IsFindByContent)
                {
                    isFindByContent = File.ReadLines(path, Encoding.Default).Any(
                        line =>
                        line.IndexOf(this.SearchContent, StringComparison.CurrentCultureIgnoreCase) >= 0);
                }
                return (isFindByName || isFindByContent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            return false;
        }

        /// <summary>
        /// Count files from directory and subdirectories.
        /// </summary>
        /// <param name="path">Path to the base directory.</param>
        private int GetFilesCount(string path)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            return dirInfo.EnumerateDirectories()
                       .AsParallel()
                       .SelectMany(di => di.EnumerateFiles("*", SearchOption.AllDirectories))
                       .Count();
        }
    }
}
