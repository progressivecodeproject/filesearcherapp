﻿namespace FileSearcherApp
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tPathRootDir = new System.Windows.Forms.TextBox();
            this.bSelectFolder = new System.Windows.Forms.Button();
            this.cIsSearchByName = new System.Windows.Forms.CheckBox();
            this.tPatternName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cIsSearchByContent = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tPatternContent = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lCurrentFile = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lFoundFiles = new System.Windows.Forms.Label();
            this.progressSearch = new System.Windows.Forms.ProgressBar();
            this.lPastTime = new System.Windows.Forms.Label();
            this.lProcFiles = new System.Windows.Forms.Label();
            this.lAllFiles = new System.Windows.Forms.Label();
            this.bSearchState = new System.Windows.Forms.Button();
            this.bStartSearch = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path to the directory where the search begins:";
            // 
            // tPathRootDir
            // 
            this.tPathRootDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tPathRootDir.Location = new System.Drawing.Point(7, 23);
            this.tPathRootDir.Name = "tPathRootDir";
            this.tPathRootDir.Size = new System.Drawing.Size(210, 20);
            this.tPathRootDir.TabIndex = 1;
            // 
            // bSelectFolder
            // 
            this.bSelectFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bSelectFolder.BackColor = System.Drawing.Color.MediumTurquoise;
            this.bSelectFolder.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSeaGreen;
            this.bSelectFolder.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Turquoise;
            this.bSelectFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSelectFolder.Location = new System.Drawing.Point(223, 21);
            this.bSelectFolder.Name = "bSelectFolder";
            this.bSelectFolder.Size = new System.Drawing.Size(39, 23);
            this.bSelectFolder.TabIndex = 2;
            this.bSelectFolder.Text = "...";
            this.bSelectFolder.UseVisualStyleBackColor = false;
            this.bSelectFolder.Click += new System.EventHandler(this.bSelectFolder_Click);
            // 
            // cIsSearchByName
            // 
            this.cIsSearchByName.AutoSize = true;
            this.cIsSearchByName.Location = new System.Drawing.Point(7, 49);
            this.cIsSearchByName.Name = "cIsSearchByName";
            this.cIsSearchByName.Size = new System.Drawing.Size(119, 17);
            this.cIsSearchByName.TabIndex = 3;
            this.cIsSearchByName.Text = "Search file by name";
            this.cIsSearchByName.UseVisualStyleBackColor = true;
            // 
            // tPatternName
            // 
            this.tPatternName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tPatternName.Location = new System.Drawing.Point(144, 72);
            this.tPatternName.Name = "tPatternName";
            this.tPatternName.Size = new System.Drawing.Size(118, 20);
            this.tPatternName.TabIndex = 4;
            this.tPatternName.Text = "*.txt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pattern to search by name:";
            // 
            // cIsSearchByContent
            // 
            this.cIsSearchByContent.AutoSize = true;
            this.cIsSearchByContent.Location = new System.Drawing.Point(7, 98);
            this.cIsSearchByContent.Name = "cIsSearchByContent";
            this.cIsSearchByContent.Size = new System.Drawing.Size(129, 17);
            this.cIsSearchByContent.TabIndex = 6;
            this.cIsSearchByContent.Text = "Search file by content";
            this.cIsSearchByContent.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Text to search by file content:";
            // 
            // tPatternContent
            // 
            this.tPatternContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tPatternContent.Location = new System.Drawing.Point(7, 134);
            this.tPatternContent.Name = "tPatternContent";
            this.tPatternContent.Size = new System.Drawing.Size(255, 20);
            this.tPatternContent.TabIndex = 7;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lCurrentFile});
            this.statusStrip1.Location = new System.Drawing.Point(0, 339);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(584, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(69, 17);
            this.toolStripStatusLabel1.Text = "Current file:";
            // 
            // lCurrentFile
            // 
            this.lCurrentFile.Name = "lCurrentFile";
            this.lCurrentFile.Size = new System.Drawing.Size(500, 17);
            this.lCurrentFile.Spring = true;
            this.lCurrentFile.Text = "null";
            this.lCurrentFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.bSearchState);
            this.panel1.Controls.Add(this.bStartSearch);
            this.panel1.Controls.Add(this.bSelectFolder);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tPathRootDir);
            this.panel1.Controls.Add(this.tPatternContent);
            this.panel1.Controls.Add(this.cIsSearchByName);
            this.panel1.Controls.Add(this.cIsSearchByContent);
            this.panel1.Controls.Add(this.tPatternName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(269, 339);
            this.panel1.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lFoundFiles);
            this.groupBox1.Controls.Add(this.progressSearch);
            this.groupBox1.Controls.Add(this.lPastTime);
            this.groupBox1.Controls.Add(this.lProcFiles);
            this.groupBox1.Controls.Add(this.lAllFiles);
            this.groupBox1.Location = new System.Drawing.Point(7, 232);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(255, 97);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Information";
            // 
            // lFoundFiles
            // 
            this.lFoundFiles.AutoSize = true;
            this.lFoundFiles.Location = new System.Drawing.Point(3, 42);
            this.lFoundFiles.Name = "lFoundFiles";
            this.lFoundFiles.Size = new System.Drawing.Size(61, 13);
            this.lFoundFiles.TabIndex = 13;
            this.lFoundFiles.Text = "Found files:";
            // 
            // progressSearch
            // 
            this.progressSearch.Location = new System.Drawing.Point(6, 71);
            this.progressSearch.Name = "progressSearch";
            this.progressSearch.Size = new System.Drawing.Size(243, 19);
            this.progressSearch.TabIndex = 12;
            // 
            // lPastTime
            // 
            this.lPastTime.AutoSize = true;
            this.lPastTime.Location = new System.Drawing.Point(3, 55);
            this.lPastTime.Name = "lPastTime";
            this.lPastTime.Size = new System.Drawing.Size(53, 13);
            this.lPastTime.TabIndex = 12;
            this.lPastTime.Text = "Past time:";
            // 
            // lProcFiles
            // 
            this.lProcFiles.AutoSize = true;
            this.lProcFiles.Location = new System.Drawing.Point(3, 29);
            this.lProcFiles.Name = "lProcFiles";
            this.lProcFiles.Size = new System.Drawing.Size(81, 13);
            this.lProcFiles.TabIndex = 12;
            this.lProcFiles.Text = "Processed files:";
            // 
            // lAllFiles
            // 
            this.lAllFiles.AutoSize = true;
            this.lAllFiles.Location = new System.Drawing.Point(3, 16);
            this.lAllFiles.Name = "lAllFiles";
            this.lAllFiles.Size = new System.Drawing.Size(31, 13);
            this.lAllFiles.TabIndex = 12;
            this.lAllFiles.Text = "Files:";
            // 
            // bSearchState
            // 
            this.bSearchState.BackColor = System.Drawing.Color.MediumTurquoise;
            this.bSearchState.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSeaGreen;
            this.bSearchState.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Turquoise;
            this.bSearchState.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSearchState.Location = new System.Drawing.Point(7, 196);
            this.bSearchState.Name = "bSearchState";
            this.bSearchState.Size = new System.Drawing.Size(255, 30);
            this.bSearchState.TabIndex = 10;
            this.bSearchState.Text = "Pause";
            this.bSearchState.UseVisualStyleBackColor = false;
            this.bSearchState.Click += new System.EventHandler(this.bSearchState_Click);
            // 
            // bStartSearch
            // 
            this.bStartSearch.BackColor = System.Drawing.Color.MediumTurquoise;
            this.bStartSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSeaGreen;
            this.bStartSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Turquoise;
            this.bStartSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bStartSearch.Location = new System.Drawing.Point(7, 160);
            this.bStartSearch.Name = "bStartSearch";
            this.bStartSearch.Size = new System.Drawing.Size(255, 30);
            this.bStartSearch.TabIndex = 9;
            this.bStartSearch.Text = "Start new searching for files";
            this.bStartSearch.UseVisualStyleBackColor = false;
            this.bStartSearch.Click += new System.EventHandler(this.bStartSearch_Click);
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(269, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(315, 339);
            this.treeView1.TabIndex = 11;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "MainForm";
            this.Text = "FileSearcherApp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tPathRootDir;
        private System.Windows.Forms.Button bSelectFolder;
        private System.Windows.Forms.CheckBox cIsSearchByName;
        private System.Windows.Forms.TextBox tPatternName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cIsSearchByContent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tPatternContent;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button bSearchState;
        private System.Windows.Forms.Button bStartSearch;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lCurrentFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar progressSearch;
        private System.Windows.Forms.Label lPastTime;
        private System.Windows.Forms.Label lProcFiles;
        private System.Windows.Forms.Label lAllFiles;
        private System.Windows.Forms.Label lFoundFiles;
    }
}

