﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FileSearcherApp
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// MainForm initialization.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            fileSearcher = new FileSearcher();
            fileSearcher.eventStart = eventStart;
            fileSearcher.eventPause = eventPause;
            fileSearcher.eventResume = eventResume;
            fileSearcher.eventEnd = eventEnd;
            fileSearcher.eventProcessing = eventProcessing;
            fileSearcher.eventSuccess = eventSuccess;

            tPathRootDir.Text = FileSearcher._StartDirectory;
            tPatternName.Text = FileSearcher._SearchPattern;
            tPatternContent.Text = FileSearcher._SearchContent;
            cIsSearchByName.Checked = FileSearcher._IsFindByName;
            cIsSearchByContent.Checked = FileSearcher._IsFindByContent;
        }

        /// <summary>
        /// Сlass declaration for finding files.
        /// </summary>
        private FileSearcher fileSearcher { get; set; }
        
        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateSearchInfo(new FSEventData(), false);
        }
        
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            fileSearcher.Dispose();
            FileSearcher._StartDirectory = tPathRootDir.Text;
            FileSearcher._SearchPattern = tPatternName.Text;
            FileSearcher._SearchContent = tPatternContent.Text;
            FileSearcher._IsFindByName = cIsSearchByName.Checked;
            FileSearcher._IsFindByContent = cIsSearchByContent.Checked;
        }

        /// <summary>
        /// Root directory selection dialog.
        /// </summary>
        private void bSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                tPathRootDir.Text = dialog.SelectedPath;
            }
        }

        /// <summary>
        /// Start new search files.
        /// </summary>
        private void bStartSearch_Click(object sender, EventArgs e)
        {
            try
            {
                treeView1.Nodes.Clear();
                bSearchState.Text = "Pause";
                fileSearcher.StartDirectory = tPathRootDir.Text;
                fileSearcher.SearchPattern = tPatternName.Text;
                fileSearcher.SearchContent = tPatternContent.Text;
                fileSearcher.IsFindByName = cIsSearchByName.Checked;
                fileSearcher.IsFindByContent = cIsSearchByContent.Checked;
                fileSearcher.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Set pause or resume files search.
        /// </summary>
        private void bSearchState_Click(object sender, EventArgs e)
        {
            try
            {
                fileSearcher.AutoPauseOrResume();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Updating the displayed data from the search information.
        /// </summary>
        /// <param name="data">The search info.</param>
        /// <param name="bSearchState">The state of the button "pause" or "resume".</param>
        /// <param name="bSearchStateText">The text of the button "pause" or "resume".</param>
        private void UpdateSearchInfo(FSEventData data, bool bSearchState = true, string bSearchStateText = null)
        {
            this.Invoke(new Action(() =>
            {
                this.bSearchState.Enabled = bSearchState;
                this.bSearchState.BackColor = bSearchState ? Color.MediumTurquoise : Color.Gray;
                if (bSearchStateText != null) this.bSearchState.Text = bSearchStateText;
                this.lAllFiles.Text = string.Format("Files: {0}", data.FilesAllCount);
                this.lProcFiles.Text = string.Format("Processed files: {0}", data.CheckedFiles);
                this.lFoundFiles.Text = string.Format("Found files: {0}", data.FoundFiles);
                this.lPastTime.Text = string.Format("Past time: {0}", data.Time.Elapsed);
                this.lCurrentFile.Text = data.Path;
                this.progressSearch.Maximum = data.FilesAllCount;
                this.progressSearch.Value = data.CheckedFiles;
            }));
        }

        /// <summary>
        /// Event on begin of the search.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventStart(FSEventData data)
        {
            try
            {
                UpdateSearchInfo(data, bSearchStateText: "Pause");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Event on pause of the search.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventPause(FSEventData data)
        {
            try
            {
                UpdateSearchInfo(data, bSearchStateText: "Resume");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Event on resume of the search.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventResume(FSEventData data)
        {
            try
            {
                UpdateSearchInfo(data, bSearchStateText: "Pause");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Event on end of the search.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventEnd(FSEventData data)
        {
            try
            {
                UpdateSearchInfo(data, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// File handling event.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventProcessing(FSEventData data)
        {
            try
            {
                UpdateSearchInfo(data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// Successful event in the search files.
        /// </summary>
        /// <param name="data">The search info.</param>
        private void eventSuccess(FSEventData data)
        {
            try
            {
                string[] split = data.Path.Split('\\');
                TreeNodeCollection nodes = treeView1.Nodes;
                TreeNode[] nodesFind = null;
                TreeNode node = null;
                for (int i = 0; i < split.Length; i++)
                {
                    nodesFind = nodes.Find(split[i], false);
                    if (nodesFind.Length > 0)
                        nodes = nodesFind[0].Nodes;
                    else this.Invoke(new Action(() => nodes = (node = nodes.Add(split[i], split[i])).Nodes));
                }
                this.Invoke(new Action(() =>
                {
                    if (node != null)
                        node.BackColor = Color.LightGreen;
                    treeView1.ExpandAll();
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
